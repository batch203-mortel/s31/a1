/*
-What directive is used by Node.js in loading the modules it needs?
	directive method
- What Node.js module contains a method for server creation?
	http module
- What is the method of the http object responsible for creating a server using Node.js?
	createServer() method
- What method of the response object allows us to set status codes and content types?
	writeHead
- Where will console.log() output its contents when run in Node.js?
	terminal
- What property of the request object contains the address's endpoint?
	url
*/





let http = require("http");

const port = 4000;

const server = http.createServer((request,response)=> {
		if(request.url == "/login"){
			response.writeHead(200, {"Content-Type" : "plain/text"})
			response.end("Welcome to the login page!");
		}

		else{
			response.writeHead(200, {"Content-Type" : "plain/text"})
			response.end("Sorry the page tha you're trying to access cannot be found!");
		}

});


server.listen(port);
console.log('Server is successfully running in localhost:3000');